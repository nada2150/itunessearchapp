const initialState = {
  resultList: [],
  trackToDisplay: [],
  myFavorite: {},
};
const reducer = (state = initialState, action) => {
  if (action.type === "UPDATE_SEARCH_LIST") {
    return {
      ...state,
      resultList: action.value,
    };
  }
  if (action.type === "CLICK_TRACK") {
    return {
      ...state,
      trackToDisplay: [action.value],
    };
  }
  if (action.type === "ADD_TO_FAVORTIE") {
    let favoriteList = state.myFavorite;
    if (!favoriteList[action.value[0].trackId]) {
      favoriteList[action.value[0].trackId] = action.value[0];
    }
    return {
      ...state,
      myFavorite: favoriteList,
    };
  }
  if (action.type === "REMOVE_FROM_FAVORTIE") {
    let DeleteFavoriteList = Object.assign({}, state.myFavorite);
    delete DeleteFavoriteList[action.value];
    return {
      ...state,
      myFavorite: DeleteFavoriteList,
    };
  }
  return state;
};

export default reducer;
