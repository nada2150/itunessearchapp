import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/styles";
import { connect } from "react-redux";
import GridList from "@material-ui/core/GridList";
import "./myFavorite.css";
const styles = (theme) => ({
  root: {
    minWidth: 345,
    maxWidth: 345,
    minHeight: 400,
    maxHeight: 400,
    margin: "20px",
  },
  media: {
    height: 140,
  },
  container: {
    display: "flex",
    margin: "15px",
    height: "1000px",
  },
  grid: {
    backgroundColor: "transparent",
  },
});

class MyFavorite extends Component {
  removeFromList = (data) => {
    this.props.removeFromFavorite(data.currentTarget.id);
  };
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.container}>
        <GridList cols={4} className={classes.grid}>
          {Object.keys(this.props.myfavoriteList).length != 0 ? (
            Object.keys(this.props.myfavoriteList).map((favoriteItem) => (
              <Card className={classes.root}>
                <CardActionArea>
                  <CardMedia
                    className={classes.media}
                    image={this.props.myfavoriteList[favoriteItem].artworkUrl60}
                    title={this.props.myfavoriteList[favoriteItem].trackName}
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                      {this.props.myfavoriteList[favoriteItem].trackName}
                    </Typography>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      component="p"
                    >
                      <b>kind: </b>{" "}
                      {this.props.myfavoriteList[favoriteItem].kind}
                      <br />
                      <b>release Date: </b>{" "}
                      {this.props.myfavoriteList[favoriteItem].releaseDate}
                      <br />
                      <b>collection Price: </b>{" "}
                      {this.props.myfavoriteList[favoriteItem].collectionPrice}{" "}
                      $
                    </Typography>
                  </CardContent>
                </CardActionArea>
                <CardActions>
                  <Button
                    size="small"
                    color="primary"
                    id={favoriteItem}
                    onClick={(event) => {
                      this.removeFromList(event);
                    }}
                  >
                    Remove
                  </Button>
                  <Button
                    size="small"
                    color="primary"
                    target="_blank"
                    rel="noopener noreferrer"
                    href={this.props.myfavoriteList[favoriteItem].trackViewUrl}
                  >
                    See on Itunes
                  </Button>
                </CardActions>
              </Card>
            ))
          ) : (
            <p className="noResult">Nothing to Show</p>
          )}
        </GridList>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    myfavoriteList: state.myFavorite,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onFoundMatch: (arr) => dispatch({ type: "UPDATE_SEARCH_LIST", value: arr }),
    removeFromFavorite: (favoriteItem) =>
      dispatch({ type: "REMOVE_FROM_FAVORTIE", value: favoriteItem }),
    addToFavorite: (favoriteItem) =>
      dispatch({ type: "ADD_TO_FAVORTIE", value: favoriteItem }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles, { withTheme: true })(MyFavorite));
