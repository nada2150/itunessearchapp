import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import Logo from "../../images/itunesApiLogo.png";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    "& > *": {
      margin: theme.spacing(20),
      marginLeft: theme.spacing(75),
    },
  },
  large: {
    width: theme.spacing(50),
    height: theme.spacing(50),
  },
}));

export default function ImageAvatars() {
  const classes = useStyles();

  return (
    <div>
      <div className={classes.root}>
        <Avatar alt="Remy Sharp" src={Logo} className={classes.large} />
      </div>
    </div>
  );
}
