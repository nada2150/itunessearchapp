import React from "react";
import SearchResultList from "../searchMenu/searchResultList/searchResultList";
import SearchInput from "./searchInput/searchInput";
import "./searchMenu.css";

function SearchMenu() {
  return (
    <div className="searchMenu">
      <SearchInput></SearchInput>
      <SearchResultList></SearchResultList>
    </div>
  );
}

export default SearchMenu;
