import React, { Component } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import GetResult from "../../../Services/serverCommunicater/serverCommunicater";
import { connect } from "react-redux";

class BasicTextFields extends Component {
  inputSearch = (value) => {
    GetResult.serverCommunicater(value)
      .then((response) => {
        this.props.onFoundMatch(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  render() {
    const useStyles = makeStyles((theme) => ({
      root: {
        "& > *": {
          margin: theme.spacing(1),
          width: "10px",
        },
      },
    }));
    let classes = useStyles;
    return (
      <form className={classes.root} noValidate autoComplete="off">
        <TextField
          id="outlined-basic"
          label="Search"
          variant="outlined"
          onChange={(event) => {
            this.inputSearch(event.currentTarget.value);
          }}
        />
      </form>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    ctr: state.resultList,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onFoundMatch: (arr) => dispatch({ type: "UPDATE_SEARCH_LIST", value: arr }),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(BasicTextFields);
