import React, { Component } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import { withStyles } from "@material-ui/styles";
import { connect } from "react-redux";

class SelectedListItem extends Component {
  openTrack = (elment) => {
    let trackID = elment.currentTarget.id;
    let trackData = this.props.resultList[trackID];
    this.props.onClickTrack(trackData);
  };
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <List component="nav" aria-label="secondary mailbox folder">
          {this.props.resultList.map((result, key) => (
            <div>
              <ListItem
                button
                id={key}
                onClick={(event) => {
                  this.openTrack(event);
                }}
              >
                <ListItemText primary={result.trackName} />
              </ListItem>
              <Divider />
            </div>
          ))}
        </List>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    resultList: state.resultList,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onClickTrack: (data) => dispatch({ type: "CLICK_TRACK", value: data }),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(mapStateToProps)(SelectedListItem));
