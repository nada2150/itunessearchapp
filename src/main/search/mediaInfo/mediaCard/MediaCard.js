import React, { Component } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/styles";
import Avatar from "@material-ui/core/Avatar";
import ReactAudioPlayer from "react-audio-player";
import "./MediaCard.css";
import "react-jinke-music-player/assets/index.css";
import Tooltip from "@material-ui/core/Tooltip";
import Button from "@material-ui/core/Button";

const styles = makeStyles((theme) => ({
  root: {
    display: "none",
    height: "100%",
  },
  details: {
    display: "flex",
    flexDirection: "column",
  },
  content: {
    flex: "1 0 auto",
  },
  cover: {
    width: 151,
  },
  large: {
    display: "none",
  },
  controls: {
    display: "flex",
    alignItems: "center",
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
  playIcon: {
    height: 38,
    width: 38,
  },
}));

class MediaControlCard extends Component {
  addToFavorite = () => {
    let itemToAdd = this.props.trackToDisplayProps;
    this.props.addToFavorite(itemToAdd);
  };
  render() {
    const { classes } = this.props;
    return this.props.trackToDisplayProps
      ? this.props.trackToDisplayProps.map((itemToDisplay) => (
          <div className={classes.details}>
            <div>
              <Card className="Card">
                <CardContent className={classes.content}>
                  <Typography component="h5" variant="h5">
                    {itemToDisplay.trackName}
                  </Typography>
                  <Typography variant="subtitle1" color="textSecondary">
                    {itemToDisplay.artistName}
                  </Typography>
                  <Avatar
                    alt="Remy Sharp"
                    src={itemToDisplay.artworkUrl100}
                    className={classes.large}
                  />
                </CardContent>
              </Card>
            </div>
            <div className="buttonContianer">
              <Tooltip title="Add to Favorite">
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={this.addToFavorite}
                >
                  Add To Favorite
                </Button>
              </Tooltip>
            </div>
            <img className="imgLogo" src={itemToDisplay.artworkUrl100} />
            <ReactAudioPlayer
              src={itemToDisplay.previewUrl}
              controls
              className="musicPlayer"
            />
          </div>
        ))
      : null;
  }
}
const mapStateToProps = (state) => {
  return {
    trackToDisplayProps: state.trackToDisplay,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onFoundMatch: (arr) => dispatch({ type: "UPDATE_SEARCH_LIST", value: arr }),
    addToFavorite: (favoriteItem) =>
      dispatch({ type: "ADD_TO_FAVORTIE", value: favoriteItem }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(MediaControlCard));
