import React from "react";
import SearchMenu from "./searchMenu/searchMenu";
import MediaInfo from "./mediaInfo/mediaInfo";
import "./search.css";

function search() {
  return (
    <div className="Search">
      <SearchMenu></SearchMenu>
      <MediaInfo></MediaInfo>
    </div>
  );
}

export default search;
