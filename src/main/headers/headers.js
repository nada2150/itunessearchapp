import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Search from "../search/search";
import Favorite from "../myFavorite/myFavorite";
import "./headers.css";
import Home from "../home/home";

export default function headers() {
  return (
    <Router>
      <div>
        <nav className="haeders">
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/search">Search</Link>
            </li>
            <li>
              <Link to="/favorite">My favorite</Link>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route path="/search">
            <Search></Search>
          </Route>
          <Route path="/favorite">
            <Favorite></Favorite>
          </Route>
          <Route path="/">
            <Home></Home>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
