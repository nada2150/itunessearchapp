const httprequest = require("../httpRequest/httpRequest");
let serverUrl = "http://localhost:8000";
let getDataFromServer = (value) => {
  return httprequest.get(serverUrl + "/itunesApi", {
    params: {
      searchINout: value,
    },
  });
};

exports.serverCommunicater = getDataFromServer;
