const express = require("express");
const app = express();
const getItunesData = require("./ItunesApi/getItunesDataApi");
let cors = require("cors");
app.use(cors());

app.get("/itunesApi", function (req, res) {
  let valueToSearch = req.query.searchINout;
  getItunesData
    .searchQueryInItunesApi(valueToSearch)
    .then((response) => {
      res.send(response.data.results);
    })
    .catch((error) => {
      console.log(error);
    });
});

app.listen(8000, () =>
  console.log(`Example app listening at http://localhost:8000`)
);
