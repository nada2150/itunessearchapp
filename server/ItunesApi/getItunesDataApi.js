const httprequest = require("../../src/main/Services/httpRequest/httpRequest");
let itunesApiUrl = "https://itunes.apple.com/search?term=";

let searchQueryInItunesApi = (query, config) => {
  return httprequest.get(itunesApiUrl + query, config);
};
exports.searchQueryInItunesApi = searchQueryInItunesApi;
